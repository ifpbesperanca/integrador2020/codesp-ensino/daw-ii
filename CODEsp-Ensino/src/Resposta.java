import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Resposta implements Identificavel {

	@Id
	@GeneratedValue(generator = "resposta_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "resposta_seq", sequenceName = "resposta_seq")

	private int id;
	private Date data;
	private String conteudo;

	@ManyToOne
	@JoinColumn(name = "id_aluno")
	Aluno a;
	
	@ManyToOne
	@JoinColumn(name = "id_atividade")
	Atividade at;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resposta other = (Resposta) obj;
		if (conteudo == null) {
			if (other.conteudo != null)
				return false;
		} else if (!conteudo.equals(other.conteudo))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Resposta [id=" + id + ", data=" + data + ", conteudo=" + conteudo + "]";
	}

	public Resposta(int id, Date data, String conteudo) {
		super();
		this.id = id;
		this.data = data;
		this.conteudo = conteudo;
	}

}
