import java.io.File;
import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Atividade implements Identificavel{

	@Id
	@GeneratedValue(generator = "atividade_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "atividade_seq", sequenceName = "atividade_seq")

	private int id;
	private String conteudo;
	private File apoio;

	@ManyToMany
	@JoinTable(name = "Atividade_Aluno", joinColumns = @JoinColumn(name = "Atividade_id"), inverseJoinColumns = @JoinColumn(name = "Aluno_id")

	)
	Set<Aluno> aluno;

	@ManyToOne
	@JoinColumn(name = "id_coach")
	Coach c;

	@OneToMany(mappedBy = "at")
	private Set<Resposta> resposta;
	
	@ManyToMany
	@JoinTable(name = "Atividade_Aluno", joinColumns = @JoinColumn(name = "Atividade_id"), inverseJoinColumns = @JoinColumn(name = "Aluno_id")

	)
	Set<Aluno> aluno2;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public File getApoio() {
		return apoio;
	}

	public void setApoio(File apoio) {
		this.apoio = apoio;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (apoio == null) {
			if (other.apoio != null)
				return false;
		} else if (!apoio.equals(other.apoio))
			return false;
		if (conteudo == null) {
			if (other.conteudo != null)
				return false;
		} else if (!conteudo.equals(other.conteudo))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Atividade [id=" + id + ", conteudo=" + conteudo + ", apoio=" + apoio + "]";
	}

	public Atividade(int id, String conteudo, File apoio) {
		super();
		this.id = id;
		this.conteudo = conteudo;
		this.apoio = apoio;
	}

}
