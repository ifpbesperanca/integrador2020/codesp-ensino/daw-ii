import java.io.File;
import java.sql.Date;

public interface Interface {
	
	public Orientador addOrientador(String nome, String email, String senha, long contato) throws NomeInvalidoException, EmailInvalidoException;
	
	public Orientador removeOrientador(String nome, String email, String senha) throws NomeInvalidoException, EmailInvalidoException;
	
	public Coach addCoach(String nome, String email, String senha, long contato) throws NomeInvalidoException, EmailInvalidoException;
	
	public Coach removeCoach(String nome, String email, String senha) throws NomeInvalidoException, EmailInvalidoException;
	
	public Aluno addAluno(String nome, String email, String senha) throws NomeInvalidoException, EmailInvalidoException;
	
	public Aluno removeAluno(String nome, String email, String senha) throws NomeInvalidoException, EmailInvalidoException;
	
	public Escola addEscola(String nome, String endereco, long contato) throws NomeInvalidoException;
	
	public Escola removeEscola(String nome) throws NomeInvalidoException;
	
	public Atividade addAtividade (File apoio, String conteudo, Coach nome) throws NomeInvalidoException;
	
	public Atividade atualizaAtividade (File apoio, String conteudo, Coach nome) throws NomeInvalidoException;
	
	public Atividade removeAtividade (File apoio, String conteudo, Coach nome) throws NomeInvalidoException;
	
	public Duvida addDuvida (String conteudo, Date datapergunta, String pergunta, Date dataresposta, String resposta) throws NomeInvalidoException;
	
	public Duvida atualizaDuvida (String conteudo, Date datapergunta, String pergunta, Date dataresposta, String resposta ) throws NomeInvalidoException;
	
	public Duvida removeDuvida (String conteudo, Date datapergunta, String pergunta, Date dataresposta, String resposta) throws ConteudoInvalidoException;
	
	public Resposta addResposta (Date data, String Conteudo) throws ConteudoInvalidoException;
		
	public Resposta atualizaResposta (Date data, String Conteudo) throws ConteudoInvalidoException;

	public Resposta removeResposta (Date data, String Conteudo) throws ConteudoInvalidoException;
	
	

}
