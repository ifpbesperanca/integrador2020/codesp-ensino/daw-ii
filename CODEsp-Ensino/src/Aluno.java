import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Aluno implements Identificavel {

	@Id
	@GeneratedValue(generator = "aluno_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "aluno_seq", sequenceName = "aluno_seq")

	private int id;
	private String nome;
	private String email;
	private String senha;

	@ManyToOne
	@JoinColumn(name = "id_escola")
	Aluno a;

	@OneToMany(mappedBy = "d")
	private Set<Duvida> duvida;

	@ManyToMany
	@JoinTable(name = "Aluno_Atividade", joinColumns = @JoinColumn(name = "Aluno_id"), inverseJoinColumns = @JoinColumn(name = "Atividade_id")

	)
	Set<Atividade> atividade;

	@OneToMany(mappedBy = "a")
	private Set<Resposta> resposta;

	@ManyToOne
	@JoinColumn(name = "id_coach")
	Coach c;
	
	@ManyToMany
	@JoinTable(name = "Aluno_Atividade", joinColumns = @JoinColumn(name = "Aluno_id"), inverseJoinColumns = @JoinColumn(name = "Atividade_id")

	)
	Set<Atividade> atividade2;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [id=" + id + ", nome=" + nome + ", email=" + email + ", senha=" + senha + "]";
	}

	public Aluno(int id, String nome, String email, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}

}
