import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Duvida  implements Identificavel{

	@Id
	@GeneratedValue(generator = "duvida_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "duvida_seq", sequenceName = "duvida_seq")

	private int id;
	private String conteudo;
	private Date datapergunta;
	private String pergunta;
	private Date dataresposta;
	private String resposta;

	@ManyToOne
	@JoinColumn(name = "id_aluno")
	Duvida d;

	@ManyToOne
	@JoinColumn(name = "id_coach")
	Coach c;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Date getDatapergunta() {
		return datapergunta;
	}

	public void setDatapergunta(Date datapergunta) {
		this.datapergunta = datapergunta;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public Date getDataresposta() {
		return dataresposta;
	}

	public void setDataresposta(Date dataresposta) {
		this.dataresposta = dataresposta;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Duvida other = (Duvida) obj;
		if (conteudo == null) {
			if (other.conteudo != null)
				return false;
		} else if (!conteudo.equals(other.conteudo))
			return false;
		if (datapergunta == null) {
			if (other.datapergunta != null)
				return false;
		} else if (!datapergunta.equals(other.datapergunta))
			return false;
		if (dataresposta == null) {
			if (other.dataresposta != null)
				return false;
		} else if (!dataresposta.equals(other.dataresposta))
			return false;
		if (id != other.id)
			return false;
		if (pergunta == null) {
			if (other.pergunta != null)
				return false;
		} else if (!pergunta.equals(other.pergunta))
			return false;
		if (resposta == null) {
			if (other.resposta != null)
				return false;
		} else if (!resposta.equals(other.resposta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Duvida [id=" + id + ", conteudo=" + conteudo + ", datapergunta=" + datapergunta + ", pergunta="
				+ pergunta + ", dataresposta=" + dataresposta + ", resposta=" + resposta + "]";
	}

	public Duvida(int id, String conteudo, Date datapergunta, String pergunta, Date dataresposta, String resposta) {
		super();
		this.id = id;
		this.conteudo = conteudo;
		this.datapergunta = datapergunta;
		this.pergunta = pergunta;
		this.dataresposta = dataresposta;
		this.resposta = resposta;
	}

}
